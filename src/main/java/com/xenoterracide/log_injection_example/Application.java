package com.xenoterracide.log_injection_example;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class Application {
    private static final Logger log = LoggerFactory.getLogger( Application.class );

    public static void main( final String... args ) throws Exception {
        log.info( "STARTING" );
        new Runner().run( args );
        SpringApplication.run( Application.class, args );
    }

    @Component
    static class Runner implements CommandLineRunner {

        @Override
        public void run( final String... args ) throws Exception {
            log.info( "running with args: {}", args );
            log.debug( "running '{}'", args );
            try {
                throw new IllegalArgumentException( Arrays.toString( args ) );
            }
            catch ( Exception e ) {
                log.error( "", e );
            }
        }
    }
}
