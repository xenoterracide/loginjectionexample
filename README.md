## INSTALL
    mvn package
## RUN
as intended without injection

    java -jar target/log-injection-example-1.0-SNAPSHOT.jar hello

with injection

    java -jar target/log-injection-example-1.0-SNAPSHOT.jar "
    11:45:21.873 [main] INFO com.xenoterracide.log_injection_example.Application - running with args: bar"
